package com.zuitt.b193;


import java.util.InputMismatchException;
import java.util.Scanner;

public class ActivitySolution1 {
    public static void main(String [] args){

        Scanner input = new Scanner(System.in);
        int factorial = 1;

        try {
            System.out.println("Please insert a number to be computed:");

        }
        catch (InputMismatchException e){
            System.out.println("Input is not a number");
        }
        catch (Exception e){
            System.out.println("Invalid Input");
        }
        finally {
            int num = input.nextInt();
            for(int i = 1; i <= num; i++) {
                factorial *= i;
            }
            System.out.println("The factorial of " + num + " is " + factorial);
        }
        }

}
